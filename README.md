# Vulkan Renderer

This is a learning project where I experiment with Vulkan, discover how works and how to implement common techniques and examples I know in DirectX and OpenGL.
I started from [vulkan-tutorial.com](https://vulkan-tutorial.com) and I'd like to develop a framework which would resemble [DirectX examples](https://github.com/Aldarrion/DirectXExamples)
is some ways, but which would be closer to an engine or a renderer with scene switching to unleash as much potential of the Vulkan API as I can.

## Getting started

Requirements

* [LunarG Vulkan SDK](https://vulkan.lunarg.com/sdk/home) version 1.1.82.0
* Environment variable `VULKAN_SDK` set to the LunarG SDK root folder (where `Uninstall.exe` is)
* Visual studio 2017
* [Git LFS](https://git-lfs.github.com/) (*optional* - if you want to clone)

Then simply open the `vkRenderer.sln`, build, and run. Shaders are compiled automatically by the `buildShaders.bat` script and all required assets and shaders are automatically copied to the directory with the `vkRenderer.exe` in "Custom build step" in VS.
