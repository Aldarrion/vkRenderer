#pragma once

#include <GLFW/glfw3.h>
#include <vulkan/vulkan.h>

struct RendererState {
    VkInstance VulkanInstance = nullptr;
    VkDevice Device = nullptr;
    VkPhysicalDevice PhysicalDevice = nullptr;
    VkCommandPool CommandPool = nullptr;
    GLFWwindow* Window = nullptr;
    VkQueue GraphicsQueue = nullptr;

    static RendererState& inst() {
        static RendererState instance;
        return instance;
    }
};
