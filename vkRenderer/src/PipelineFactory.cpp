#include "PipelineFactory.h"

VkViewport PipelineFactory::viewport_;
VkPipelineColorBlendAttachmentState PipelineFactory::colorBlendAttachment_;
VkRect2D PipelineFactory::scissor_;
