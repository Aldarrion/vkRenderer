//#define GLM_FORCE_LEFT_HANDED
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include "PerspectiveCamera.h"
#include "Time.h"

#include <glm/gtc/matrix_transform.hpp>
#include <stdexcept>
#include "VkrUtil.h"


const glm::vec3 PerspectiveCamera::WORLD_UP = glm::vec3(0.0f, -1.0f, 0.0f);

void PerspectiveCamera::move(const Directions direction) {
    glm::vec3 moveVec;
    switch (direction) {
        case Directions::UP: moveVec = WORLD_UP; break;
        case Directions::DOWN: moveVec = -WORLD_UP; break;
        case Directions::LEFT: moveVec = -right_; break;
        case Directions::RIGHT: moveVec = right_; break;
        case Directions::FRONT: moveVec = forward_; break;
        case Directions::BACK: moveVec = -forward_;  break;
        default: throw std::runtime_error("Invalid movement direction.");
    }
    
    position_ += moveVec * (Time::inst().delta() * speed_);
}

void PerspectiveCamera::mouseMovement(float xoffset, float yoffset) {
    xoffset *= mouseSensitivity_;
    yoffset *= mouseSensitivity_;

    yaw_ += xoffset;
    pitch_ -= yoffset;
    
    if (pitch_ > 89.0f)
        pitch_ = 89.0f;
    if (pitch_ < -89.0f)
        pitch_ = -89.0f;

    updateVectors();
}

void PerspectiveCamera::updateVectors() {
    forward_.x = glm::cos(glm::radians(pitch_)) * glm::sin(glm::radians(yaw_));
    forward_.y = glm::sin(glm::radians(pitch_));
    forward_.z = glm::cos(glm::radians(pitch_)) * glm::cos(glm::radians(yaw_));
    forward_ = glm::normalize(forward_);

    right_ = glm::normalize(glm::cross(forward_, WORLD_UP));
    up_ = glm::normalize(glm::cross(right_, forward_));
}

glm::mat4 PerspectiveCamera::getVPMatrix() const {
    return getProjectionMatrix() * getViewMatrix();
}

glm::mat4 PerspectiveCamera::getViewMatrix() const {
    return glm::lookAt(position_, position_ + forward_, up_);
}

glm::mat4 PerspectiveCamera::getProjectionMatrix() const {
    auto projection = glm::perspectiveRH(glm::radians(fov_), aspectRatio_, near_, far_);
    projection[1][1] *= -1; // Vulkan has Y upside down
    return projection;
}
