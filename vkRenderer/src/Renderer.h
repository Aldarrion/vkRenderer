#pragma once

#include "Vertex.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>
#include <glm/glm.hpp>
#include <array>
#include "PerspectiveCamera.h"
#include "PipelineFactory.h"
#include "Transform.h"
#include "Box.h"

class Renderer {
public:
    static double XOffset;
    static double YOffset;
    
    void run() {
        initWindow();
        initVulkan();
        mainLoop();
        cleanup();
    }

    Renderer();

private:
    struct QueueFamilyIndices {
        int GraphicsFamily = -1;
        int PresentFamily = -1;

        bool isComplete() const {
            return GraphicsFamily >= 0 && PresentFamily >= 0;
        }
    };

    struct SwapChainSupportDetails {
        VkSurfaceCapabilitiesKHR Capabilities;
        std::vector<VkSurfaceFormatKHR> Formats;
        std::vector<VkPresentModeKHR> PresentModes;
    };

    struct UniformBufferObject {
        glm::mat4 Model;
        glm::mat4 View;
        glm::mat4 Projection;
        glm::mat4 NormalMatrix;
    };

    struct SimpleColorUBO {
        glm::mat4 Model;
        glm::mat4 View;
        glm::mat4 Projection;
        glm::vec3 Color;
    };

    struct PointLight {
        glm::vec3 Position;
        float pad_0;
        glm::vec3 Color;
        float pad_1;
        glm::vec3 ViewPos;
    };


    constexpr static int WIDTH = 1280;
    constexpr static int HEIGHT = 720;

    constexpr static int MAX_FRAMES_IN_FLIGHT = 2;

    const std::vector<const char*> validationLayers_ = {
        "VK_LAYER_LUNARG_standard_validation"
    };

    const std::vector<const char*> deviceExtensions_ = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };

    VkPhysicalDeviceFeatures requiredPhysicalDeviceFeatures_;

    const std::vector<Vertex> vertices_ = {
        { { -0.5f, -0.5f, 0.5f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, -1.0f }, { 1.0f, 0.0f } },
        { { 0.5f, -0.5f, 0.5f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f, -1.0f }, { 0.0f, 0.0f } },
        { { 0.5f, 0.5f, 0.5f }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, -1.0f }, { 0.0f, 1.0f } },
        { { -0.5f, 0.5f, 0.5f }, { 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f, -1.0f }, { 1.0f, 1.0f } }
    };

    const std::vector<uint16_t> indices_ = {
        0, 1, 2,
        2, 3, 0,
    };

    Transform lightTransform_{ glm::vec3(0.0f, 0.0f, -4.0f), glm::vec3(), glm::vec3(0.2f, 0.2f, 0.2f) };
    glm::vec3 lightColor_{ glm::vec3(1.0f, 1.0f, 1.0f) };

#ifdef NDEBUG
    constexpr static bool ENABLE_VALIDATION_LAYERS = false;
#else
    constexpr static bool ENABLE_VALIDATION_LAYERS = true;
#endif

    VkQueue presentQueue_ = nullptr;
    VkSurfaceKHR surface_ = nullptr;
    
    // Input
    static bool isFirstMouse_;
    static double lastMouseX_;
    static double lastMouseY_;
    PerspectiveCamera camera_;
    
    // Swap chain
    VkSwapchainKHR swapChain_ = nullptr;
    std::vector<VkImage> swapChainImages_;
    VkFormat swapChainImageFormat_ = {};
    VkExtent2D swapChainExtent_ = {};
    std::vector<VkImageView> swapChainImageViews_;
    std::vector<VkFramebuffer> swapChainFramebuffers_;

    // Pipeline
    VkRenderPass renderPass_ = nullptr;
    VkDescriptorPool descriptorPool_ = nullptr;
    VkDescriptorSet descriptorSet_ = nullptr;
    VkDescriptorSet lightDescriptorSet_ = nullptr;
    PipelineWrapper pipelineWrapper_;
    PipelineWrapper simpleColorPipelineWrapper_;

    // Commands
    std::vector<VkCommandBuffer> commandBuffers_;

    // Drawing
    size_t frameNumber_ = 0;
    std::vector<VkSemaphore> imageAvailableSemaphores_;
    std::vector<VkSemaphore> renderFinishedSemaphores_;
    std::vector<VkFence> inFlightFences_;

    // Depth
    VkImage depthImage_ = nullptr;
    VkDeviceMemory depthImageMemory_ = nullptr;
    VkImageView depthImageView_ = nullptr;

    // Buffers
    VkBuffer vertexBuffer_ = nullptr;
    VkDeviceMemory vertexBufferMemory_ = nullptr;
    VkBuffer indexBuffer_ = nullptr;
    VkDeviceMemory indexBufferMemory_ = nullptr;
    VkBuffer uniformBuffer_ = nullptr;
    VkDeviceMemory uniformBufferMemory_ = nullptr;
    VkBuffer lightUniformBuffer_ = nullptr;
    VkDeviceMemory lightUniformBufferMemory_ = nullptr;
    
    Box box_;

    // Lights
    VkBuffer pointLight_ = nullptr;
    VkDeviceMemory pointLightMemory_ = nullptr;

    // Texturing
    uint32_t mipLevels_;
    VkImage textureImage_ = nullptr;
    VkDeviceMemory textureImageMemory_ = nullptr;
    VkImageView textureImageView_ = nullptr;
    VkSampler textureSampler_ = nullptr;

    // Debug
    VkDebugReportCallbackEXT debugReportCallback_ = nullptr;


    //========
    // Methods
    // =======

    void initWindow();
    
    // Vulkan init
    void initVulkan();
    void createInstance();
    void createSurface();
    bool checkValidationLayerSupport() const;
    std::vector<const char*> getRequiredExtensions() const;
    void setupDebugCallback();
    void pickPhysicalDevice();
    bool isPhysicalDeviceSuitable(VkPhysicalDevice physicalDevice);
    bool checkDeviceExtensionSupport(VkPhysicalDevice device) const;
    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice physicalDevice) const;
    void createLogicalDevice();
    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device) const;
    static VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
    static VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) const;
    void createSwapChain();
    void createImageViews();
    //void createDescriptorSetLayout();
    //void createGraphicsPipeline();
    //void createRenderPass();
    void createFrameBuffers();
    void createCommandPool();
    VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling,
                                 VkFormatFeatureFlags features) const;
    VkFormat findDepthFormat() const;
    static bool hasStencilComponent(VkFormat format);
    void createDepthResources();
    void createImage(
        uint32_t width, 
        uint32_t height,
        uint32_t mipLevels,
        VkFormat format, 
        VkImageTiling tiling, 
        VkImageUsageFlags usage,
        VkMemoryPropertyFlags properties, 
        VkImage& image, 
        VkDeviceMemory& imageMemory
    ) const;
    void transitionImageLayout(
        VkImage image, 
        VkFormat format, 
        VkImageLayout oldLayout, 
        VkImageLayout newLayout,
        uint32_t mipLevels
    ) const;
    void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) const;
    void createTextureImage();
    void generateMipmaps(VkImage image, int32_t texWidth, int32_t texHeight, uint32_t mipLevels) const;
    void createTextureImageView();
    VkImageView createImageView(VkImage, VkFormat, VkImageAspectFlags, uint32_t) const;
    void createTextureSampler();
    void createUniformBuffers();
    void createLightBuffer();
    void createDescriptorPool();
    void createBoxDescriptorSet();
    void createDescriptorSet();
    void createCommandBuffers();
    void createSyncObjects();
    void recreateSwapChain();

    // Shaders
    //static std::vector<char> readFile(const std::string& filename);
    //VkShaderModule createShaderModule(const std::vector<char>& code) const;

    // Main
    void mainLoop();
    void update();
    void updateUniformBuffer() const;
    void drawFrame();

    // Input
    static void mouseCallback(GLFWwindow*, double, double);
    void processInput();

    // Cleanup
    void cleanupSwapChain();
    void cleanup();

    // Debug
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT objType,
        uint64_t obj,
        size_t location,
        int32_t code,
        const char* layerPrefix,
        const char* msg,
        void* userData
    );
};
