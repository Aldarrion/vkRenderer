#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include "Renderer.h"
#include "Proxies.h"
#include "VkrUtil.h"
#include "Time.h"
#include "RenderPassFactory.h"

#include <stdexcept>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_set>
#include <fstream>
#include <glm/gtc/matrix_transform.hpp>
#include <chrono>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#include "RendererState.h"

double Renderer::XOffset = 0.0; 
double Renderer::YOffset = 0.0;

bool Renderer::isFirstMouse_ = true;
double Renderer::lastMouseX_ = 0.0;
double Renderer::lastMouseY_ = 0.0;

Renderer::Renderer() 
        : requiredPhysicalDeviceFeatures_({})
        , camera_(glm::vec3(0.0f, 0.0f, -2.0f)) {
    requiredPhysicalDeviceFeatures_.samplerAnisotropy = VK_TRUE;
    requiredPhysicalDeviceFeatures_.fillModeNonSolid = VK_TRUE;
}

void Renderer::initWindow() {
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    RendererState::inst().Window = glfwCreateWindow(WIDTH, HEIGHT, "vkRenderer", nullptr, nullptr);

    glfwSetInputMode(RendererState::inst().Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(RendererState::inst().Window, mouseCallback);
}

void Renderer::initVulkan() {
    createInstance();
    setupDebugCallback();
    createSurface(); // Call right after instance creation (or debug callback setup).
    pickPhysicalDevice();
    createLogicalDevice();
    createSwapChain();
    createImageViews();
    //createRenderPass();
    renderPass_ = RenderPassFactory::createRenderPass(RendererState::inst().Device, swapChainImageFormat_, findDepthFormat());
    //createDescriptorSetLayout();
    //createGraphicsPipeline();
    pipelineWrapper_ = PipelineFactory::createPipelineWrapper(RendererState::inst().Device, renderPass_, swapChainExtent_);
    simpleColorPipelineWrapper_ = PipelineFactory::createSimpleColorPipelineWrapper(RendererState::inst().Device, renderPass_, swapChainExtent_);

    createCommandPool();
    
    createDepthResources();
    createTextureImage();
    createTextureImageView();
    createTextureSampler();

    createFrameBuffers(); // Must be sometime after depth resources
    
    createVertexBuffer(vertexBuffer_, vertexBufferMemory_, vertices_.data(), vertices_.size() * sizeof(vertices_[0]));
    createIndexBuffer(indexBuffer_, indexBufferMemory_, indices_.data(), indices_.size() * sizeof(indices_[0]));
    
    // Create box
    box_ = Box::makeBox();

    createUniformBuffers();
    createLightBuffer();
    createDescriptorPool();
    createDescriptorSet();
    createBoxDescriptorSet();
    createCommandBuffers();
    createSyncObjects();
}

void Renderer::createInstance() {
    if (ENABLE_VALIDATION_LAYERS && !checkValidationLayerSupport()) {
        throw std::runtime_error("Validation layers requested, but not available!");
    }

    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "vkRenderer";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;
    //appInfo.apiVersion = VK_API_VERSION_1_1;

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    // Request required extensions
    auto extensions = getRequiredExtensions();
    createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();

    // Enable validation layers
    if (ENABLE_VALIDATION_LAYERS) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers_.size());
        createInfo.ppEnabledLayerNames = validationLayers_.data();
    } else {
        createInfo.enabledLayerCount = 0;
    }

    // Create the instance
    checkResult(
        vkCreateInstance(&createInfo, nullptr, &RendererState::inst().VulkanInstance), 
        "Failed to create vulkan instance!"
    );

    // See which extensions are available
    /*uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
    std::vector<VkExtensionProperties> availableExtensions(extensionCount);
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, availableExtensions.data());

    std::cout << "Available extensions:" << std::endl;

    for (const auto& extension : availableExtensions) {
        std::cout << "    " << extension.extensionName << std::endl;
    }*/
}

void Renderer::createSurface() {
    // Platform independent window creation
    checkResult(
        glfwCreateWindowSurface(RendererState::inst().VulkanInstance, RendererState::inst().Window, nullptr, &surface_),
        "Failed to create window surface!"
    );
}

bool Renderer::checkValidationLayerSupport() const {
    // List available layers
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    // Check if we have all the layers we want
    for (const char* layerName : validationLayers_) {
        const auto found = std::find_if(
            std::begin(availableLayers), 
            std::end(availableLayers), 
            [&layerName](const auto& item) { return item.layerName == layerName; }
        );

        if (found != availableLayers.end()) {
            return false;
        }
    }

    return true;
}

std::vector<const char*> Renderer::getRequiredExtensions() const {
    uint32_t glfwExtensionCount = 0;
    const char ** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

    if (ENABLE_VALIDATION_LAYERS) {
        extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    }

    return extensions;
}

void Renderer::setupDebugCallback() {
    if (!ENABLE_VALIDATION_LAYERS) {
        return;
    }

    VkDebugReportCallbackCreateInfoEXT createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
    createInfo.pfnCallback = debugCallback;

    checkResult(
        CreateDebugReportCallbackEXT(RendererState::inst().VulkanInstance, &createInfo, nullptr, &debugReportCallback_),
        "Failed to set up debug callback!"
    );
}

void Renderer::pickPhysicalDevice() {
    uint32_t physicalDeviceCount = 0;
    vkEnumeratePhysicalDevices(RendererState::inst().VulkanInstance, &physicalDeviceCount, nullptr);

    if (physicalDeviceCount == 0) {
        throw std::runtime_error("Failed to find GPUs with Vulkan support!");
    }

    std::vector<VkPhysicalDevice> physicalDevices(physicalDeviceCount);
    vkEnumeratePhysicalDevices(RendererState::inst().VulkanInstance, &physicalDeviceCount, physicalDevices.data());

    const auto suitableDevice = std::find_if(
        physicalDevices.begin(), 
        physicalDevices.end(), 
        [this](const auto item) { return this->isPhysicalDeviceSuitable(item); }
    );

    if (suitableDevice == physicalDevices.end()) {
        throw std::runtime_error("Failed to find a suitable GPU!");
    }

    RendererState::inst().PhysicalDevice = *suitableDevice;
}

bool Renderer::isPhysicalDeviceSuitable(const VkPhysicalDevice physicalDevice) {
    VkPhysicalDeviceProperties deviceProperties;
    vkGetPhysicalDeviceProperties(physicalDevice, &deviceProperties);

    VkPhysicalDeviceFeatures deviceFeatures;
    vkGetPhysicalDeviceFeatures(physicalDevice, &deviceFeatures);

    const auto indices = findQueueFamilies(physicalDevice);

    const bool areExtensionsSupported = checkDeviceExtensionSupport(physicalDevice);

    bool isSwapChainAdequate = false;
    if (areExtensionsSupported) {
        SwapChainSupportDetails swapChainSupport = querySwapChainSupport(physicalDevice);
        isSwapChainAdequate = !swapChainSupport.Formats.empty() && !swapChainSupport.PresentModes.empty();
    }

    VkPhysicalDeviceFeatures supportedFeatures;
    vkGetPhysicalDeviceFeatures(physicalDevice, &supportedFeatures);

    return 
        indices.isComplete() &&
        areExtensionsSupported &&
        isSwapChainAdequate &&
        areRequiredFeaturesSupported(supportedFeatures, requiredPhysicalDeviceFeatures_) &&
        deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
}

bool Renderer::checkDeviceExtensionSupport(VkPhysicalDevice device) const {
    uint32_t extensionCount;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

    std::vector<VkExtensionProperties> availableExtensions(extensionCount);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

    std::unordered_set<std::string> requiredExtensions(deviceExtensions_.begin(), deviceExtensions_.end());

    for (const auto& extension : availableExtensions) {
        requiredExtensions.erase(extension.extensionName);
    }

    return requiredExtensions.empty();
}

Renderer::QueueFamilyIndices Renderer::findQueueFamilies(const VkPhysicalDevice physicalDevice) const {
    QueueFamilyIndices indices;

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);
    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

    int i = 0;
    for (const auto& queueFamily : queueFamilies) {
        // TODO is this potential queue index overwriting really what we want?
        if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT ) {
            indices.GraphicsFamily = i;
        }

        VkBool32 presentSupport = VK_FALSE;
        vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface_, &presentSupport);
        if (queueFamily.queueCount > 0 && presentSupport) {
            indices.PresentFamily = i;
        }

        if (indices.isComplete()) {
            break;
        }

        i++;
    }

    return indices;
}

void Renderer::createLogicalDevice() {
    const QueueFamilyIndices indices = findQueueFamilies(RendererState::inst().PhysicalDevice);

    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    const std::unordered_set<int> uniqueQueueFamilies = { 
        indices.GraphicsFamily, 
        indices.PresentFamily 
    };

    float queuePriority = 1.0f;
    for (const int queueFamily : uniqueQueueFamilies) {
        VkDeviceQueueCreateInfo queueCreateInfo{};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamily;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pQueueCreateInfos = queueCreateInfos.data();
    
    createInfo.pEnabledFeatures = &requiredPhysicalDeviceFeatures_;

    createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions_.size());
    createInfo.ppEnabledExtensionNames = deviceExtensions_.data();

    if (ENABLE_VALIDATION_LAYERS) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers_.size());
        createInfo.ppEnabledLayerNames = validationLayers_.data();
    } else {
        createInfo.enabledLayerCount = 0;
    }

    checkResult(
        vkCreateDevice(RendererState::inst().PhysicalDevice, &createInfo, nullptr, &RendererState::inst().Device), 
        "Failed to create logical device!"
    );

    vkGetDeviceQueue(RendererState::inst().Device, indices.GraphicsFamily, 0, &RendererState::inst().GraphicsQueue);
    vkGetDeviceQueue(RendererState::inst().Device, indices.PresentFamily, 0, &presentQueue_);
}

Renderer::SwapChainSupportDetails Renderer::querySwapChainSupport(VkPhysicalDevice device) const {
    SwapChainSupportDetails details{};

    // Get capabilites
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface_, &details.Capabilities);

    // Get formats
    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &formatCount, nullptr);

    if (formatCount != 0) {
        details.Formats.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &formatCount, details.Formats.data());
    }

    // Get present modes
    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &presentModeCount, nullptr);

    if (presentModeCount != 0) {
        details.PresentModes.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &presentModeCount, details.PresentModes.data());
    }

    return details;
}

VkSurfaceFormatKHR Renderer::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
    if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) {
        return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
    }

    for (const auto& availableFormat : availableFormats) {
        if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            return availableFormat;
        }
    }

    return availableFormats[0];
}

VkPresentModeKHR Renderer::chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) {
    // VK_PRESENT_MODE_FIFO_KHR is the only one guaranteed to be available
    VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

    // VK_PRESENT_MODE_IMMEDIATE_KHR has the best compatability
    for (const auto& availablePresentMode : availablePresentModes) {
        if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
            // This is our preffered mode
            return availablePresentMode;
        } else if (availablePresentMode == VK_PRESENT_MODE_FIFO_RELAXED_KHR) {
            bestMode = availablePresentMode;
        }
    }

    return bestMode;
}

VkExtent2D Renderer::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) const {
    if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
        return capabilities.currentExtent;
    } else {
        VkExtent2D actualExtent = { WIDTH, HEIGHT };

        actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
}

void Renderer::createSwapChain() {
    const SwapChainSupportDetails swapChainSupport = querySwapChainSupport(RendererState::inst().PhysicalDevice);

    const VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.Formats);
    const VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.PresentModes);
    const VkExtent2D extent = chooseSwapExtent(swapChainSupport.Capabilities);

    uint32_t imageCount = swapChainSupport.Capabilities.minImageCount; // Do +1 here for tripple buffering
    if (swapChainSupport.Capabilities.maxImageCount > 0 // 0 is if there is no limit
            && imageCount > swapChainSupport.Capabilities.maxImageCount) {
        imageCount = swapChainSupport.Capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = surface_;

    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = swapChainImageFormat_ = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = swapChainExtent_ = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT; // VK_IMAGE_USAGE_TRANSFER_DST_BIT if not drawing directly to images

    // Queues
    const QueueFamilyIndices indices = findQueueFamilies(RendererState::inst().PhysicalDevice);
    uint32_t queueFamilyIndices[] = { 
        static_cast<uint32_t>(indices.GraphicsFamily), 
        static_cast<uint32_t>(indices.PresentFamily) 
    };

    if (indices.GraphicsFamily != indices.PresentFamily) {
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    } else { // Only a single queue
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0; // Optional
        createInfo.pQueueFamilyIndices = nullptr; // Optional
    }

    // Special transformations for each image
    createInfo.preTransform = swapChainSupport.Capabilities.currentTransform;

    // Blend with other windows in the window system?
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE; // Clip pixels if there is another window on top of this one

    // Previous swap chain - after recreation
    createInfo.oldSwapchain = nullptr;

    checkResult(
        vkCreateSwapchainKHR(RendererState::inst().Device, &createInfo, nullptr, &swapChain_),
        "Failed to create swap chain!"
    );

    vkGetSwapchainImagesKHR(RendererState::inst().Device, swapChain_, &imageCount, nullptr);
    swapChainImages_.resize(imageCount);
    vkGetSwapchainImagesKHR(RendererState::inst().Device, swapChain_, &imageCount, swapChainImages_.data());
}

void Renderer::createImageViews() {
    swapChainImageViews_.resize(swapChainImages_.size());

    for (uint32_t i = 0; i < swapChainImages_.size(); i++) {
        swapChainImageViews_[i] = createImageView(swapChainImages_[i], swapChainImageFormat_, VK_IMAGE_ASPECT_COLOR_BIT, 1);
    }
}

void Renderer::createFrameBuffers() {
    swapChainFramebuffers_.resize(swapChainImageViews_.size());
    
    for (size_t i = 0; i < swapChainImageViews_.size(); i++) {
        std::array<VkImageView, 2> attachments = {
            swapChainImageViews_[i],
            depthImageView_
        };

        VkFramebufferCreateInfo framebufferInfo{};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = renderPass_;
        framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
        framebufferInfo.pAttachments = attachments.data();
        framebufferInfo.width = swapChainExtent_.width;
        framebufferInfo.height = swapChainExtent_.height;
        framebufferInfo.layers = 1;

        checkResult(
            vkCreateFramebuffer(RendererState::inst().Device, &framebufferInfo, nullptr, &swapChainFramebuffers_[i]),
            "Failed to create framebuffer!"
        );
    }
}

void Renderer::createCommandPool() {
    const QueueFamilyIndices queueFamilyIndices = findQueueFamilies(RendererState::inst().PhysicalDevice);

    VkCommandPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndices.GraphicsFamily;
    poolInfo.flags = 0; // Optional

    checkResult(
        vkCreateCommandPool(RendererState::inst().Device, &poolInfo, nullptr, &RendererState::inst().CommandPool),
        "Failed to create command pool!"
    );
}

VkFormat Renderer::findSupportedFormat(
        const std::vector<VkFormat>& candidates, 
        const VkImageTiling tiling, 
        const VkFormatFeatureFlags features) const {
    for (const VkFormat format : candidates) {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(RendererState::inst().PhysicalDevice, format, &props);

        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
            return format;
        } else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
            return format;
        }
    }

    throw std::runtime_error("Failed to find supported format!");
}

VkFormat Renderer::findDepthFormat() const {
    return findSupportedFormat(
        { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    );
}

bool Renderer::hasStencilComponent(const VkFormat format) {
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

void Renderer::createDepthResources() {
    const VkFormat depthFormat = findDepthFormat();

    createImage(
        swapChainExtent_.width, 
        swapChainExtent_.height,
        1,
        depthFormat, 
        VK_IMAGE_TILING_OPTIMAL, 
        VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, 
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
        depthImage_, 
        depthImageMemory_
    );
    depthImageView_ = createImageView(depthImage_, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, 1);

    transitionImageLayout(
        depthImage_, 
        depthFormat, 
        VK_IMAGE_LAYOUT_UNDEFINED, 
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        1
    );
}

void Renderer::createImage(
        const uint32_t width,
        const uint32_t height,
        const uint32_t mipLevels,
        const VkFormat format,
        const VkImageTiling tiling,
        const VkImageUsageFlags usage,
        const VkMemoryPropertyFlags properties, 
        VkImage& image, 
        VkDeviceMemory& imageMemory) const {
    VkImageCreateInfo imageInfo{};
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width = width;
    imageInfo.extent.height = height;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = mipLevels;
    imageInfo.arrayLayers = 1;
    imageInfo.format = format;
    imageInfo.tiling = tiling;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage = usage;
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    checkResult(
        vkCreateImage(RendererState::inst().Device, &imageInfo, nullptr, &image),
        "Failed to create image!"
    );

    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(RendererState::inst().Device, image, &memRequirements);

    VkMemoryAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

    checkResult(
        vkAllocateMemory(RendererState::inst().Device, &allocInfo, nullptr, &imageMemory),
        "Failed to allocate image memory!"
    );

    vkBindImageMemory(RendererState::inst().Device, image, imageMemory, 0);
}

void Renderer::transitionImageLayout(
        const VkImage image, 
        const VkFormat format, 
        const VkImageLayout oldLayout,
        const VkImageLayout newLayout,
        const uint32_t mipLevels) const {
    const VkCommandBuffer commandBuffer = beginSingleTimeCommands();

    VkImageMemoryBarrier barrier{};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;

    if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

        if (hasStencilComponent(format)) {
            barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
        }
    } else {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    }

    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = mipLevels;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;
    
    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;

    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    } else {
        throw std::invalid_argument("Unsupported layout transition!");
    }

    vkCmdPipelineBarrier(
        commandBuffer,
        sourceStage, destinationStage,
        0,
        0, nullptr,
        0, nullptr,
        1, &barrier
    );

    endSingleTimeCommands(commandBuffer);
}

void Renderer::copyBufferToImage(
        const VkBuffer buffer, 
        const VkImage image, 
        const uint32_t width, 
        const uint32_t height) const {
    const VkCommandBuffer commandBuffer = beginSingleTimeCommands();

    VkBufferImageCopy region{};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;

    region.imageOffset = { 0, 0, 0 };
    region.imageExtent = {
        width,
        height,
        1
    };

    vkCmdCopyBufferToImage(
        commandBuffer,
        buffer,
        image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region
    );

    endSingleTimeCommands(commandBuffer);
}

void Renderer::createTextureImage() {
    int texWidth, texHeight, texChannels;
    stbi_uc* pixels = stbi_load("textures/container2.png", &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
    const VkDeviceSize imageSize = texWidth * texHeight * 4;

    if (!pixels) {
        throw std::runtime_error("Failed to load texture image!");
    }

    mipLevels_ = static_cast<uint32_t>(std::floor(std::log2(std::max(texWidth, texHeight)))) + 1;

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    createBuffer(
        imageSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
        stagingBuffer, 
        stagingBufferMemory
    );

    void* data;
    vkMapMemory(RendererState::inst().Device, stagingBufferMemory, 0, imageSize, 0, &data);
    memcpy(data, pixels, static_cast<size_t>(imageSize));
    vkUnmapMemory(RendererState::inst().Device, stagingBufferMemory);

    stbi_image_free(pixels);

    createImage(
        texWidth,
        texHeight,
        mipLevels_,
        VK_FORMAT_R8G8B8A8_UNORM, 
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, 
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
        textureImage_, 
        textureImageMemory_
    );

    transitionImageLayout(textureImage_, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, mipLevels_);
    copyBufferToImage(stagingBuffer, textureImage_, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));
    // Transition to shader optimal will happen while generating mipmaps

    generateMipmaps(textureImage_, texWidth, texHeight, mipLevels_);

    vkDestroyBuffer(RendererState::inst().Device, stagingBuffer, nullptr);
    vkFreeMemory(RendererState::inst().Device, stagingBufferMemory, nullptr);
}

void Renderer::generateMipmaps(
        const VkImage image, 
        const int32_t texWidth, 
        const int32_t texHeight, 
        const uint32_t mipLevels) const {
    const VkCommandBuffer commandBuffer = beginSingleTimeCommands();

    VkImageMemoryBarrier barrier = {};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.image = image;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;
    barrier.subresourceRange.levelCount = 1;

    int32_t mipWidth = texWidth;
    int32_t mipHeight = texHeight;

    for (uint32_t i = 1; i < mipLevels; i++) {
        barrier.subresourceRange.baseMipLevel = i - 1;
        barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

        vkCmdPipelineBarrier(commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
            0, nullptr,
            0, nullptr,
            1, &barrier);

        VkImageBlit blit = {};
        blit.srcOffsets[0] = { 0, 0, 0 };
        blit.srcOffsets[1] = { mipWidth, mipHeight, 1 };
        blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        blit.srcSubresource.mipLevel = i - 1;
        blit.srcSubresource.baseArrayLayer = 0;
        blit.srcSubresource.layerCount = 1;
        blit.dstOffsets[0] = { 0, 0, 0 };
        blit.dstOffsets[1] = { mipWidth / 2, mipHeight / 2, 1 };
        blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        blit.dstSubresource.mipLevel = i;
        blit.dstSubresource.baseArrayLayer = 0;
        blit.dstSubresource.layerCount = 1;

        vkCmdBlitImage(commandBuffer,
            image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1, &blit,
            VK_FILTER_LINEAR);

        barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        vkCmdPipelineBarrier(commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
            0, nullptr,
            0, nullptr,
            1, &barrier);

        if (mipWidth > 1) mipWidth /= 2;
        if (mipHeight > 1) mipHeight /= 2;
    }

    barrier.subresourceRange.baseMipLevel = mipLevels - 1;
    barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    vkCmdPipelineBarrier(commandBuffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
        0, nullptr,
        0, nullptr,
        1, &barrier);

    endSingleTimeCommands(commandBuffer);
}

void Renderer::createTextureImageView() {
    textureImageView_ = createImageView(textureImage_, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT, mipLevels_);
}

VkImageView Renderer::createImageView(
        const VkImage image, 
        const VkFormat format, 
        const VkImageAspectFlags aspectFlags,
        const uint32_t mipLevels) const {
    VkImageViewCreateInfo viewInfo = {};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = mipLevels;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    VkImageView imageView;
    checkResult(
        vkCreateImageView(RendererState::inst().Device, &viewInfo, nullptr, &imageView),
        "Failed to create texture image view!"
    );

    return imageView;
}

void Renderer::createTextureSampler() {
    VkSamplerCreateInfo samplerInfo{};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy = 16;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = static_cast<float>(mipLevels_);

    checkResult(
        vkCreateSampler(RendererState::inst().Device, &samplerInfo, nullptr, &textureSampler_),
        "Failed to create texture sampler!"
    );
}

void Renderer::createUniformBuffers() {
    createBuffer(
        sizeof(UniformBufferObject),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, 
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
        uniformBuffer_, 
        uniformBufferMemory_
    );

    createBuffer(
        sizeof(SimpleColorUBO),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        lightUniformBuffer_,
        lightUniformBufferMemory_
    );
}

void Renderer::createLightBuffer() {
    createBuffer(
        sizeof(PointLight),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        pointLight_,
        pointLightMemory_
    );
}

void Renderer::createDescriptorPool() {
    std::array<VkDescriptorPoolSize, 2> poolSizes{};
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount = 3;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[1].descriptorCount = 1;

    VkDescriptorPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
    poolInfo.pPoolSizes = poolSizes.data();
    poolInfo.maxSets = 2;

    checkResult(
        vkCreateDescriptorPool(RendererState::inst().Device, &poolInfo, nullptr, &descriptorPool_),
        "Failed to create descriptor pool!"
    );
}

void Renderer::createBoxDescriptorSet() {
    VkDescriptorSetLayout layouts[] = { simpleColorPipelineWrapper_.DescriptorSetLayout };
    VkDescriptorSetAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = descriptorPool_;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = layouts;

    checkResult(
        vkAllocateDescriptorSets(RendererState::inst().Device, &allocInfo, &lightDescriptorSet_),
        "Failed to allocate descriptor set!"
    );

    VkDescriptorBufferInfo bufferInfo{};
    bufferInfo.buffer = lightUniformBuffer_;
    bufferInfo.offset = 0;
    bufferInfo.range = sizeof(SimpleColorUBO);

    std::array<VkWriteDescriptorSet, 1> descriptorWrites{};
    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = lightDescriptorSet_;
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].dstArrayElement = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &bufferInfo;

    vkUpdateDescriptorSets(
        RendererState::inst().Device,
        static_cast<uint32_t>(descriptorWrites.size()),
        descriptorWrites.data(),
        0, nullptr
    );
}

void Renderer::createDescriptorSet() {
    VkDescriptorSetLayout layouts[] = { pipelineWrapper_.DescriptorSetLayout };
    VkDescriptorSetAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = descriptorPool_;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = layouts;

    checkResult(
        vkAllocateDescriptorSets(RendererState::inst().Device, &allocInfo, &descriptorSet_),
        "Failed to allocate descriptor set!"
    );

    VkDescriptorBufferInfo bufferInfo{};
    bufferInfo.buffer = uniformBuffer_;
    bufferInfo.offset = 0;
    bufferInfo.range = sizeof(UniformBufferObject);

    VkDescriptorImageInfo imageInfo{};
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imageInfo.imageView = textureImageView_;
    imageInfo.sampler = textureSampler_;

    VkDescriptorBufferInfo lightInfo{};
    lightInfo.buffer = pointLight_;
    lightInfo.offset = 0;
    lightInfo.range = sizeof(PointLight);

    std::array<VkWriteDescriptorSet, 3> descriptorWrites{};
    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = descriptorSet_;
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].dstArrayElement = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &bufferInfo;

    descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[1].dstSet = descriptorSet_;
    descriptorWrites[1].dstBinding = 1;
    descriptorWrites[1].dstArrayElement = 0;
    descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrites[1].descriptorCount = 1;
    descriptorWrites[1].pImageInfo = &imageInfo;

    descriptorWrites[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[2].dstSet = descriptorSet_;
    descriptorWrites[2].dstBinding = 2;
    descriptorWrites[2].dstArrayElement = 0;
    descriptorWrites[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[2].descriptorCount = 1;
    descriptorWrites[2].pBufferInfo = &lightInfo;

    vkUpdateDescriptorSets(
        RendererState::inst().Device, 
        static_cast<uint32_t>(descriptorWrites.size()), 
        descriptorWrites.data(), 
        0, nullptr
    );
}

void Renderer::createCommandBuffers() {
    commandBuffers_.resize(swapChainFramebuffers_.size());

    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = RendererState::inst().CommandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers_.size());

    checkResult(
        vkAllocateCommandBuffers(RendererState::inst().Device, &allocInfo, commandBuffers_.data()),
        "Failed to allocate command buffers!"
    );

    for (size_t i = 0; i < commandBuffers_.size(); i++) {
        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        beginInfo.pInheritanceInfo = nullptr; // Optional

        checkResult(
            vkBeginCommandBuffer(commandBuffers_[i], &beginInfo),
            "Failed to begin recording command buffer!"
        );

        VkRenderPassBeginInfo renderPassInfo{};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass = renderPass_;
        renderPassInfo.framebuffer = swapChainFramebuffers_[i];
        renderPassInfo.renderArea.offset = { 0, 0 };
        renderPassInfo.renderArea.extent = swapChainExtent_;
        
        std::array<VkClearValue, 2> clearValues{};
        clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[1].depthStencil = { 1.0f, 0 };

        renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
        renderPassInfo.pClearValues = clearValues.data();;

        // Rendering
        vkCmdBeginRenderPass(commandBuffers_[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
        
        vkCmdBindPipeline(commandBuffers_[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineWrapper_.Pipeline);
        
        VkBuffer vertexBuffers[] = { vertexBuffer_ };
        VkDeviceSize offsets[] = { 0 };
        vkCmdBindVertexBuffers(commandBuffers_[i], 0, 1, vertexBuffers, offsets);
        vkCmdBindIndexBuffer(commandBuffers_[i], indexBuffer_, 0, VK_INDEX_TYPE_UINT16);

        vkCmdBindDescriptorSets(
            commandBuffers_[i],
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipelineWrapper_.PipelineLayout,
            0, 1, &descriptorSet_,
            0, nullptr
        );

        //vkCmdDraw(commandBuffers_[i], static_cast<uint32_t>(vertices_.size()), 1, 0, 0);
        vkCmdDrawIndexed(commandBuffers_[i], static_cast<uint32_t>(indices_.size()), 1, 0, 0, 0);
       

        // =====
        // Light
        // =====
        vkCmdBindPipeline(commandBuffers_[i], VK_PIPELINE_BIND_POINT_GRAPHICS, simpleColorPipelineWrapper_.Pipeline);

        vkCmdBindDescriptorSets(
            commandBuffers_[i],
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            simpleColorPipelineWrapper_.PipelineLayout,
            0, 1, &lightDescriptorSet_,
            0, nullptr
        );

        box_.draw(commandBuffers_[i]);


        vkCmdEndRenderPass(commandBuffers_[i]);

        checkResult(
            vkEndCommandBuffer(commandBuffers_[i]),
            "Failed to record command buffer!"
        );
    }
}

void Renderer::createSyncObjects() {
    imageAvailableSemaphores_.resize(MAX_FRAMES_IN_FLIGHT);
    renderFinishedSemaphores_.resize(MAX_FRAMES_IN_FLIGHT);
    inFlightFences_.resize(MAX_FRAMES_IN_FLIGHT);

    VkSemaphoreCreateInfo semaphoreInfo{};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
        if (vkCreateSemaphore(RendererState::inst().Device, &semaphoreInfo, nullptr, &imageAvailableSemaphores_[i]) != VK_SUCCESS
                || vkCreateSemaphore(RendererState::inst().Device, &semaphoreInfo, nullptr, &renderFinishedSemaphores_[i]) != VK_SUCCESS
                || vkCreateFence(RendererState::inst().Device, &fenceInfo, nullptr, &inFlightFences_[i]) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create semaphores!");
        }
    }
}

void Renderer::recreateSwapChain() {
    vkDeviceWaitIdle(RendererState::inst().Device);

    cleanupSwapChain();

    createSwapChain();
    createImageViews();
    //createRenderPass();
    renderPass_ = RenderPassFactory::createRenderPass(RendererState::inst().Device, swapChainImageFormat_, findDepthFormat());
    //createGraphicsPipeline();
    pipelineWrapper_ = PipelineFactory::createPipelineWrapper(RendererState::inst().Device, renderPass_, swapChainExtent_);

    createDepthResources();
    createFrameBuffers();
    createCommandBuffers();
}

void Renderer::mainLoop() {
    while (!glfwWindowShouldClose(RendererState::inst().Window) &&
            glfwGetKey(RendererState::inst().Window, GLFW_KEY_ESCAPE) != GLFW_PRESS) {
        glfwPollEvents();

        update();
        drawFrame();
    }
    
    // Wait for async operations in draw frame to finish
    vkDeviceWaitIdle(RendererState::inst().Device);
}

float rotation = 0.0f;

void Renderer::update() {
    Time::inst().update();
    glfwSetWindowTitle(RendererState::inst().Window, ("vkRenderer | Frame time: " + std::to_string(Time::inst().delta())).c_str());

    processInput();

    updateUniformBuffer();

    PointLight pl{};
    pl.Position = lightTransform_.Position;
    pl.Color = lightColor_;
    pl.ViewPos = camera_.getPosition();

    void* data;
    vkMapMemory(RendererState::inst().Device, pointLightMemory_, 0, sizeof(pl), 0, &data);
    memcpy(data, &pl, sizeof(pl));
    vkUnmapMemory(RendererState::inst().Device, pointLightMemory_);
}

void Renderer::updateUniformBuffer() const {
    {
        rotation += 90.0f * Time::inst().delta();

        UniformBufferObject ubo{};
        ubo.Model = glm::rotate(glm::mat4(1.0f), glm::radians(rotation), glm::vec3(0.0f, 0.0f, 1.0f));
        ubo.View = glm::mat4(1.0f);
        ubo.Projection = camera_.getVPMatrix();
        ubo.NormalMatrix = glm::mat4(1.0f); //computeNormalMatrix(ubo.Model);
    
        void* data;
        vkMapMemory(RendererState::inst().Device, uniformBufferMemory_, 0, sizeof(ubo), 0, &data);
        memcpy(data, &ubo, sizeof(ubo));
        vkUnmapMemory(RendererState::inst().Device, uniformBufferMemory_);
    }

    // Light UBO
    {
        SimpleColorUBO ubo{};
        ubo.Model = lightTransform_.generateModelMatrix();
        ubo.View = glm::mat4(1.0f);
        ubo.Projection = camera_.getVPMatrix();
        ubo.Color = lightColor_;
    
        void* data;
        vkMapMemory(RendererState::inst().Device, lightUniformBufferMemory_, 0, sizeof(ubo), 0, &data);
        memcpy(data, &ubo, sizeof(ubo));
        vkUnmapMemory(RendererState::inst().Device, lightUniformBufferMemory_);
    }
}

void Renderer::drawFrame() {
    vkWaitForFences(RendererState::inst().Device, 1, &inFlightFences_[frameNumber_], VK_TRUE, std::numeric_limits<uint64_t>::max());
    vkResetFences(RendererState::inst().Device, 1, &inFlightFences_[frameNumber_]);

    uint32_t imageIndex;
    const auto resultAcquire = vkAcquireNextImageKHR(RendererState::inst().Device, swapChain_, std::numeric_limits<uint64_t>::max(), imageAvailableSemaphores_[frameNumber_], nullptr, &imageIndex);
    if (resultAcquire == VK_ERROR_OUT_OF_DATE_KHR) {
        recreateSwapChain();
        return;
    } else if (resultAcquire != VK_SUCCESS && resultAcquire != VK_SUBOPTIMAL_KHR) {
        throw std::runtime_error("Failed to acquire swap chain image, error: " + std::to_string(resultAcquire));
    }

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    VkSemaphore waitSemaphores[] = { imageAvailableSemaphores_[frameNumber_] };
    VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffers_[imageIndex];
    VkSemaphore signalSemaphores[] = { renderFinishedSemaphores_[frameNumber_] };
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    checkResult(
        vkQueueSubmit(RendererState::inst().GraphicsQueue, 1, &submitInfo, inFlightFences_[frameNumber_]),
        "Failed to submit draw command buffer"
    );

    VkPresentInfoKHR presentInfo{};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;
    VkSwapchainKHR swapChains[] = { swapChain_ };
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr; // Optional

    vkQueuePresentKHR(presentQueue_, &presentInfo);

    frameNumber_ = (frameNumber_ + 1) % MAX_FRAMES_IN_FLIGHT;
}

void Renderer::mouseCallback(GLFWwindow*, const double xpos, const double ypos) {
    if (isFirstMouse_) {
        lastMouseX_ = xpos;
        lastMouseY_ = ypos;
        isFirstMouse_ = false;
    }

    XOffset = xpos - lastMouseX_;
    YOffset = lastMouseY_ - ypos;
    lastMouseX_ = xpos;
    lastMouseY_ = ypos;
}

void Renderer::processInput() {
    camera_.mouseMovement(static_cast<float>(XOffset), static_cast<float>(YOffset));
    XOffset = 0.0;
    YOffset = 0.0;

    if (glfwGetKey(RendererState::inst().Window, GLFW_KEY_W) == GLFW_PRESS) {
        camera_.move(Directions::FRONT);
    } 
    if (glfwGetKey(RendererState::inst().Window, GLFW_KEY_S) == GLFW_PRESS) {
        camera_.move(Directions::BACK);
    } 
    if (glfwGetKey(RendererState::inst().Window, GLFW_KEY_A) == GLFW_PRESS) {
        camera_.move(Directions::LEFT);
    } 
    if (glfwGetKey(RendererState::inst().Window, GLFW_KEY_D) == GLFW_PRESS) {
        camera_.move(Directions::RIGHT);
    } 
    if (glfwGetKey(RendererState::inst().Window, GLFW_KEY_SPACE) == GLFW_PRESS) {
        camera_.move(Directions::UP);
    } 
    if (glfwGetKey(RendererState::inst().Window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
        camera_.move(Directions::DOWN);
    }
}

void Renderer::cleanupSwapChain() {
    vkDestroyImageView(RendererState::inst().Device, depthImageView_, nullptr);
    vkDestroyImage(RendererState::inst().Device, depthImage_, nullptr);
    vkFreeMemory(RendererState::inst().Device, depthImageMemory_, nullptr);

    for (const auto& swapChainFramebuffer : swapChainFramebuffers_) {
        vkDestroyFramebuffer(RendererState::inst().Device, swapChainFramebuffer, nullptr);
    }

    vkFreeCommandBuffers(RendererState::inst().Device, RendererState::inst().CommandPool, static_cast<uint32_t>(commandBuffers_.size()), commandBuffers_.data());

    vkDestroyPipeline(RendererState::inst().Device, simpleColorPipelineWrapper_.Pipeline, nullptr);
    vkDestroyPipelineLayout(RendererState::inst().Device, simpleColorPipelineWrapper_.PipelineLayout, nullptr);
    vkDestroyPipeline(RendererState::inst().Device, pipelineWrapper_.Pipeline, nullptr);
    vkDestroyPipelineLayout(RendererState::inst().Device, pipelineWrapper_.PipelineLayout, nullptr);
    vkDestroyRenderPass(RendererState::inst().Device, renderPass_, nullptr);

    for (const auto& swapChainImageView : swapChainImageViews_) {
        vkDestroyImageView(RendererState::inst().Device, swapChainImageView, nullptr);
    }

    vkDestroySwapchainKHR(RendererState::inst().Device, swapChain_, nullptr);
}

void Renderer::cleanup() {
    cleanupSwapChain();

    vkDestroySampler(RendererState::inst().Device, textureSampler_, nullptr);
    vkDestroyImageView(RendererState::inst().Device, textureImageView_, nullptr);
    vkDestroyImage(RendererState::inst().Device, textureImage_, nullptr);
    vkFreeMemory(RendererState::inst().Device, textureImageMemory_, nullptr);

    vkDestroyDescriptorPool(RendererState::inst().Device, descriptorPool_, nullptr);
    vkDestroyDescriptorSetLayout(RendererState::inst().Device, simpleColorPipelineWrapper_.DescriptorSetLayout, nullptr);
    vkDestroyDescriptorSetLayout(RendererState::inst().Device, pipelineWrapper_.DescriptorSetLayout, nullptr);
    
    vkDestroyBuffer(RendererState::inst().Device, pointLight_, nullptr);
    vkFreeMemory(RendererState::inst().Device, pointLightMemory_, nullptr);

    vkDestroyBuffer(RendererState::inst().Device, lightUniformBuffer_, nullptr);
    vkFreeMemory(RendererState::inst().Device, lightUniformBufferMemory_, nullptr);

    vkDestroyBuffer(RendererState::inst().Device, uniformBuffer_, nullptr);
    vkFreeMemory(RendererState::inst().Device, uniformBufferMemory_, nullptr);

    box_.destroy();

    vkDestroyBuffer(RendererState::inst().Device, vertexBuffer_, nullptr);
    vkFreeMemory(RendererState::inst().Device, vertexBufferMemory_, nullptr);

    vkDestroyBuffer(RendererState::inst().Device, indexBuffer_, nullptr);
    vkFreeMemory(RendererState::inst().Device, indexBufferMemory_, nullptr);

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(RendererState::inst().Device, renderFinishedSemaphores_[i], nullptr);
        vkDestroySemaphore(RendererState::inst().Device, imageAvailableSemaphores_[i], nullptr);
        vkDestroyFence(RendererState::inst().Device, inFlightFences_[i], nullptr);
    }

    vkDestroyCommandPool(RendererState::inst().Device, RendererState::inst().CommandPool, nullptr);

    vkDestroyDevice(RendererState::inst().Device, nullptr);
    DestroyDebugReportCallbackEXT(RendererState::inst().VulkanInstance, debugReportCallback_, nullptr);
    vkDestroySurfaceKHR(RendererState::inst().VulkanInstance, surface_, nullptr);
    vkDestroyInstance(RendererState::inst().VulkanInstance, nullptr);

    glfwDestroyWindow(RendererState::inst().Window);

    glfwTerminate();
}

VkBool32 Renderer::debugCallback(
        VkDebugReportFlagsEXT /*flags*/, 
        VkDebugReportObjectTypeEXT /*objType*/, 
        uint64_t /*obj*/,
        size_t /*location*/, 
        int32_t /*code*/, 
        const char* /*layerPrefix*/, 
        const char* msg, 
        void* /*userData*/) {
    std::cerr << "VALIDATION LAYER: " << msg << std::endl;

    // Return true only when we want to test the VL themselves
    return VK_FALSE;
}
