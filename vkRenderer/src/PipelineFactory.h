#pragma once

#include "VkrUtil.h"
#include "Vertex.h"

#include <vulkan/vulkan.h>
#include <vector>
#include <fstream>

struct PipelineWrapper {
    VkPipelineLayout PipelineLayout;
    VkPipeline Pipeline;
    VkDescriptorSetLayout DescriptorSetLayout;
};

class PipelineFactory {
private:
    static std::vector<char> readFile(const std::string& filename) {
        // ate - start at the end to determine size
        std::ifstream file(filename, std::ios::ate | std::ios::binary);

        if (!file.is_open()) {
            throw std::runtime_error("Failed to open file: " + filename);
        }

        const auto fileSize = static_cast<size_t>(file.tellg());
        std::vector<char> buffer(fileSize);

        file.seekg(0);
        file.read(buffer.data(), fileSize);

        file.close();

        return buffer;
    }

    static VkShaderModule createShaderModule(const VkDevice device, const std::vector<char>& code) {
        VkShaderModuleCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = code.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

        VkShaderModule shaderModule;
        checkResult(
            vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule),
            "Failed to create shader module!"
        );

        return shaderModule;
    }

    static void createDescriptorSetLayout(const VkDevice device, PipelineWrapper& wrapper) {
        VkDescriptorSetLayoutBinding uboLayoutBinding{};
        uboLayoutBinding.binding = 0;
        uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        uboLayoutBinding.descriptorCount = 1;
        uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        uboLayoutBinding.pImmutableSamplers = nullptr; // Optional

        VkDescriptorSetLayoutBinding samplerLayoutBinding{};
        samplerLayoutBinding.binding = 1;
        samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        samplerLayoutBinding.descriptorCount = 1;
        samplerLayoutBinding.pImmutableSamplers = nullptr;
        samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

        VkDescriptorSetLayoutBinding lightLayoutBinding{};
        lightLayoutBinding.binding = 2;
        lightLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        lightLayoutBinding.descriptorCount = 1;
        lightLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

        std::array<VkDescriptorSetLayoutBinding, 3> bindings = { uboLayoutBinding, samplerLayoutBinding, lightLayoutBinding };
        VkDescriptorSetLayoutCreateInfo layoutInfo{};
        layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
        layoutInfo.pBindings = bindings.data();

        checkResult(
            vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &wrapper.DescriptorSetLayout),
            "Failed to create descriptor set layout!"
        );
    }

    static VkPipelineRasterizationStateCreateInfo createBasicRasterizer() {
        VkPipelineRasterizationStateCreateInfo rasterizer{};
        rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizer.depthClampEnable = VK_FALSE; // Is geometry beyond near-far clamped to near far? (if not, it's discarded)
        rasterizer.rasterizerDiscardEnable = VK_FALSE; // If true everything is discarded.
        rasterizer.polygonMode = VK_POLYGON_MODE_FILL; // Using any other mode requires enabling GPU feature
        //rasterizer.polygonMode = VK_POLYGON_MODE_LINE;
        rasterizer.lineWidth = 1.0f;
        rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
        rasterizer.depthBiasEnable = VK_FALSE; // Sometimes used for shadow mapping
        rasterizer.depthBiasConstantFactor = 0.0f; // Optional
        rasterizer.depthBiasClamp = 0.0f; // Optional
        rasterizer.depthBiasSlopeFactor = 0.0f; // Optional
        
        return rasterizer;
    }

    static VkViewport createBasicViewport(const VkExtent2D swapChainExtent) {
        VkViewport viewport{};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = static_cast<float>(swapChainExtent.width);
        viewport.height = static_cast<float>(swapChainExtent.height);
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        return viewport;
    }

    static VkViewport viewport_;
    static VkRect2D scissor_;
    static VkPipelineViewportStateCreateInfo createBasicViewportState(const VkExtent2D swapChainExtent) {
        viewport_ = createBasicViewport(swapChainExtent);

        scissor_ = {};
        scissor_.offset = { 0, 0 };
        scissor_.extent = swapChainExtent;

        VkPipelineViewportStateCreateInfo viewportState{};
        viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.viewportCount = 1;
        viewportState.pViewports = &viewport_;
        viewportState.scissorCount = 1;
        viewportState.pScissors = &scissor_;

        return viewportState;
    }

    static VkPipelineInputAssemblyStateCreateInfo createBasicInputAssembly() {
        VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
        inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssembly.primitiveRestartEnable = VK_FALSE;

        return inputAssembly;
    }
    
    static VkPipelineColorBlendAttachmentState colorBlendAttachment_;
    static VkPipelineColorBlendStateCreateInfo createBasicColorBlending() {
        colorBlendAttachment_ = {};
        colorBlendAttachment_.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        colorBlendAttachment_.blendEnable = VK_FALSE;
        colorBlendAttachment_.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
        colorBlendAttachment_.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
        colorBlendAttachment_.colorBlendOp = VK_BLEND_OP_ADD; // Optional
        colorBlendAttachment_.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
        colorBlendAttachment_.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
        colorBlendAttachment_.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

        VkPipelineColorBlendStateCreateInfo colorBlending{};
        colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlending.logicOpEnable = VK_FALSE;
        colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
        colorBlending.attachmentCount = 1;
        colorBlending.pAttachments = &colorBlendAttachment_;
        colorBlending.blendConstants[0] = 0.0f; // Optional
        colorBlending.blendConstants[1] = 0.0f; // Optional
        colorBlending.blendConstants[2] = 0.0f; // Optional
        colorBlending.blendConstants[3] = 0.0f; // Optional

        return colorBlending;
    }

    static VkPipelineMultisampleStateCreateInfo createBasicMultisampling() {
        VkPipelineMultisampleStateCreateInfo multisampling{};
        multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampling.sampleShadingEnable = VK_FALSE;
        multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        multisampling.minSampleShading = 1.0f; // Optional
        multisampling.pSampleMask = nullptr; // Optional
        multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
        multisampling.alphaToOneEnable = VK_FALSE; // Optional

        return multisampling;
    }

    static std::vector<VkPipelineShaderStageCreateInfo> createShaderStages(
            const VkDevice device, 
            const std::string& vertFile, 
            const std::string& fragFile) {
        // Shader stages
        // Vertex shader
        const auto vertShaderCode = readFile(vertFile);
        const VkShaderModule vertShaderModule = createShaderModule(device, vertShaderCode);

        VkPipelineShaderStageCreateInfo vertShaderStageInfo{};
        vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
        vertShaderStageInfo.module = vertShaderModule;
        vertShaderStageInfo.pName = "main";
        vertShaderStageInfo.pSpecializationInfo = nullptr; // Sets constants at pipeline creation time

        // Fragment shader
        const auto fragShaderCode = readFile(fragFile);
        const VkShaderModule fragShaderModule = createShaderModule(device, fragShaderCode);

        VkPipelineShaderStageCreateInfo fragShaderStageInfo{};
        fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        fragShaderStageInfo.module = fragShaderModule;
        fragShaderStageInfo.pName = "main";
        vertShaderStageInfo.pSpecializationInfo = nullptr; // Sets constants at pipeline creation time

        return { vertShaderStageInfo, fragShaderStageInfo };
    }

    static VkPipelineDepthStencilStateCreateInfo createBasicDepthStencil() {
        // Depth testing
        VkPipelineDepthStencilStateCreateInfo depthStencil{};
        depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthStencil.depthTestEnable = VK_TRUE;
        depthStencil.depthWriteEnable = VK_TRUE;
        depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
        depthStencil.depthBoundsTestEnable = VK_FALSE;
        depthStencil.minDepthBounds = 0.0f; // Optional
        depthStencil.maxDepthBounds = 1.0f; // Optional
        depthStencil.stencilTestEnable = VK_FALSE;
        depthStencil.front = {}; // Optional
        depthStencil.back = {}; // Optional

        return depthStencil;
    }

    static void createGraphicsPipeline(
            const VkDevice device, 
            const VkRenderPass renderPass,
            const VkExtent2D& swapChainExtent,
            const VkDescriptorSetLayout descriptorSetLayout,
            PipelineWrapper& wrapper) {
        const auto shaderStages = createShaderStages(device, "shaders/bin/BlinnPhong_vert.spv", "shaders/bin/BlinnPhong_frag.spv");

        // Fixed stages
        VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        auto bindingDescription = Vertex::getBindingDescription();
        auto attributeDescriptions = Vertex::getAttributeDescriptions();
        vertexInputInfo.vertexBindingDescriptionCount = 1;
        vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
        vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

        VkPipelineInputAssemblyStateCreateInfo inputAssembly = createBasicInputAssembly();
        VkPipelineViewportStateCreateInfo viewportState = createBasicViewportState(swapChainExtent);
        VkPipelineRasterizationStateCreateInfo rasterizer = createBasicRasterizer();
        VkPipelineMultisampleStateCreateInfo multisampling = createBasicMultisampling();
        VkPipelineColorBlendStateCreateInfo colorBlending = createBasicColorBlending();

        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = 1;
        pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
        pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
        pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

        checkResult(
            vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &wrapper.PipelineLayout),
            "Failed to create pipeline layout!"
        );

        VkPipelineDepthStencilStateCreateInfo depthStencil = createBasicDepthStencil();

        // Create the pipeline itself
        VkGraphicsPipelineCreateInfo pipelineInfo{};
        pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
        pipelineInfo.pStages = shaderStages.data();
        pipelineInfo.pVertexInputState = &vertexInputInfo;
        pipelineInfo.pInputAssemblyState = &inputAssembly;
        pipelineInfo.pViewportState = &viewportState;
        pipelineInfo.pRasterizationState = &rasterizer;
        pipelineInfo.pMultisampleState = &multisampling;
        pipelineInfo.pDepthStencilState = &depthStencil;
        pipelineInfo.pColorBlendState = &colorBlending;
        pipelineInfo.pDynamicState = nullptr; // Optional
        pipelineInfo.layout = wrapper.PipelineLayout;
        pipelineInfo.renderPass = renderPass;
        pipelineInfo.subpass = 0;
        pipelineInfo.basePipelineHandle = nullptr; // Optional
        pipelineInfo.basePipelineIndex = -1; // Optional

        checkResult(
            vkCreateGraphicsPipelines(device, nullptr, 1, &pipelineInfo, nullptr, &wrapper.Pipeline),
            "Failed to create graphics pipeline!"
        );

        // Cleanup
        for (const auto& stage : shaderStages) {
            vkDestroyShaderModule(device, stage.module, nullptr);
        }
    }

    static void createSimpleColorDescriptorSetLayout(const VkDevice device, PipelineWrapper& wrapper) {
        VkDescriptorSetLayoutBinding uboLayoutBinding{};
        uboLayoutBinding.binding = 0;
        uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        uboLayoutBinding.descriptorCount = 1;
        uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        uboLayoutBinding.pImmutableSamplers = nullptr; // Optional

        std::array<VkDescriptorSetLayoutBinding, 1> bindings = { uboLayoutBinding };
        VkDescriptorSetLayoutCreateInfo layoutInfo{};
        layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
        layoutInfo.pBindings = bindings.data();

        checkResult(
            vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &wrapper.DescriptorSetLayout),
            "Failed to create descriptor set layout!"
        );
    }

    static void createSimpleColorPipeline(
            const VkDevice device, 
            const VkRenderPass renderPass, 
            const VkExtent2D& swapChainExtent,
            VkDescriptorSetLayout descriptorSetLayout, 
            PipelineWrapper& wrapper) {
        const auto shaderStages = createShaderStages(device, "shaders/bin/SimpleColor_vert.spv", "shaders/bin/SimpleColor_frag.spv");

        // Fixed stages
        VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        auto bindingDescription = SimpleColorVertex::getBindingDescription();
        auto attributeDescriptions = SimpleColorVertex::getAttributeDescriptions();
        vertexInputInfo.vertexBindingDescriptionCount = 1;
        vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
        vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

        VkPipelineInputAssemblyStateCreateInfo inputAssembly = createBasicInputAssembly();
        VkPipelineViewportStateCreateInfo viewportState = createBasicViewportState(swapChainExtent);
        VkPipelineRasterizationStateCreateInfo rasterizer = createBasicRasterizer();
        VkPipelineMultisampleStateCreateInfo multisampling = createBasicMultisampling();
        VkPipelineColorBlendStateCreateInfo colorBlending = createBasicColorBlending();

        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = 1;
        pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
        pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
        pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

        checkResult(
            vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &wrapper.PipelineLayout),
            "Failed to create pipeline layout!"
        );

        VkPipelineDepthStencilStateCreateInfo depthStencil = createBasicDepthStencil();

        // Create the pipeline itself
        VkGraphicsPipelineCreateInfo pipelineInfo{};
        pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
        pipelineInfo.pStages = shaderStages.data();
        pipelineInfo.pVertexInputState = &vertexInputInfo;
        pipelineInfo.pInputAssemblyState = &inputAssembly;
        pipelineInfo.pViewportState = &viewportState;
        pipelineInfo.pRasterizationState = &rasterizer;
        pipelineInfo.pMultisampleState = &multisampling;
        pipelineInfo.pDepthStencilState = &depthStencil;
        pipelineInfo.pColorBlendState = &colorBlending;
        pipelineInfo.pDynamicState = nullptr; // Optional
        pipelineInfo.layout = wrapper.PipelineLayout;
        pipelineInfo.renderPass = renderPass;
        pipelineInfo.subpass = 0;
        pipelineInfo.basePipelineHandle = nullptr; // Optional
        pipelineInfo.basePipelineIndex = -1; // Optional

        checkResult(
            vkCreateGraphicsPipelines(device, nullptr, 1, &pipelineInfo, nullptr, &wrapper.Pipeline),
            "Failed to create graphics pipeline!"
        );

        // Cleanup
        for (const auto& stage : shaderStages) {
            vkDestroyShaderModule(device, stage.module, nullptr);
        }
    }

public:
    static PipelineWrapper createPipelineWrapper(
            const VkDevice device,
            const VkRenderPass renderPass,
            const VkExtent2D swapChainExtent) {
        PipelineWrapper wrapper{};

        createDescriptorSetLayout(device, wrapper);
        createGraphicsPipeline(device, renderPass, swapChainExtent, wrapper.DescriptorSetLayout, wrapper);

        return wrapper;
    }

    static PipelineWrapper createSimpleColorPipelineWrapper(
            const VkDevice device,
            const VkRenderPass renderPass,
            const VkExtent2D swapChainExtent) {
        PipelineWrapper wrapper{};

        createSimpleColorDescriptorSetLayout(device, wrapper);
        createSimpleColorPipeline(device, renderPass, swapChainExtent, wrapper.DescriptorSetLayout, wrapper);

        return wrapper;
    }
};
