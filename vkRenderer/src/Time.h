#pragma once

#include <chrono>

class Time {
private:
    using fractional_seconds_t = std::chrono::duration<float, std::chrono::seconds::period>;

    float deltaTime_ = 0.0f;
    std::chrono::high_resolution_clock::time_point lastFrameTime_ = std::chrono::high_resolution_clock::now();

    Time() = default;

public:
    static Time& inst() {
        static Time instance;
        return instance;
    }

    ~Time() = default;

    Time(const Time&) = delete;
    Time& operator=(const Time&) = delete;
    Time(Time&&) = delete;
    Time& operator=(Time&&) = delete;

    float delta() const { return deltaTime_; }

    void update() {
        using namespace std::chrono;

        const auto now = high_resolution_clock::now();
        deltaTime_ = fractional_seconds_t(now - lastFrameTime_).count();
        lastFrameTime_ = now;
    }
};
