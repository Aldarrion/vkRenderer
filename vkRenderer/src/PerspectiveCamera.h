#pragma once

#include <glm/glm.hpp>

enum class Directions {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    FRONT,
    BACK
};

class PerspectiveCamera {
private:
    static const glm::vec3 WORLD_UP;
    
    glm::vec3 position_;
    glm::vec3 forward_;
    glm::vec3 up_;
    glm::vec3 right_{};

    float pitch_;
    float yaw_;

    float fov_;
    float aspectRatio_;
    float near_;
    float far_;

    float speed_ = 5.0f;

    float mouseSensitivity_ = 0.1f;

public:
    explicit PerspectiveCamera(
        const glm::vec3& position = glm::vec3(0.0f, 0.0f, 0.0f),
        const float yaw = 0,
        const float pitch = 0,
        const float fov = 45.0f,
        const float aspectRatio = 16 / 9.0f,
        const float near = 0.01f,
        const float far = 1000.0f
    )
            : position_(position)
            , forward_(glm::vec3(0.0f, 0.0f, 1.0f))
            , up_(WORLD_UP)
            , pitch_(pitch)
            , yaw_(yaw)
            , fov_(fov)
            , aspectRatio_(aspectRatio)
            , near_(near)
            , far_(far) {
        updateVectors();
    }

    glm::vec3 getPosition() const { return position_; }

    float getFov() const { return fov_; }
    void setFov(const float fov) { fov_ = fov; }
    
    void setAspectRatio(const float aspect) { aspectRatio_ = aspect; }
    
    float getNear() const { return near_; }
    void setNear(const float near) { near_ = near; }
    
    float getFar() const { return far_; }
    void setFar(const float far) { far_ = far; }
    
    void move(Directions direction);
    void mouseMovement(float xoffset, float yoffset);
    void updateVectors();
    glm::mat4 getVPMatrix() const;
    glm::mat4 getViewMatrix() const;
    glm::mat4 getProjectionMatrix() const;
};
