#pragma once

#include <vulkan/vulkan.h>
#include <string>
#include <iostream>
#include <glm/glm.hpp>
#include <exception>
#include "RendererState.h"

inline void checkResult(const VkResult result, const std::string& message) {
    if (result != VK_SUCCESS) {
        throw std::runtime_error(message + " Error: " + std::to_string(result));
    }
}

inline bool isFeatureSupported(const VkBool32 supported, const VkBool32 required) {
    return (supported & required) == required;
}

inline bool areRequiredFeaturesSupported(
        const VkPhysicalDeviceFeatures supported, 
        const VkPhysicalDeviceFeatures required) {
    return
        isFeatureSupported(supported.robustBufferAccess, required.robustBufferAccess) &&
        isFeatureSupported(supported.fullDrawIndexUint32, required.fullDrawIndexUint32) &&
        isFeatureSupported(supported.imageCubeArray, required.imageCubeArray) &&
        isFeatureSupported(supported.independentBlend, required.independentBlend) &&
        isFeatureSupported(supported.geometryShader, required.geometryShader) &&
        isFeatureSupported(supported.tessellationShader, required.tessellationShader) &&
        isFeatureSupported(supported.sampleRateShading, required.sampleRateShading) &&
        isFeatureSupported(supported.dualSrcBlend, required.dualSrcBlend) &&
        isFeatureSupported(supported.logicOp, required.logicOp) &&
        isFeatureSupported(supported.multiDrawIndirect, required.multiDrawIndirect) &&
        isFeatureSupported(supported.drawIndirectFirstInstance, required.drawIndirectFirstInstance) &&
        isFeatureSupported(supported.depthClamp, required.depthClamp) &&
        isFeatureSupported(supported.depthBiasClamp, required.depthBiasClamp) &&
        isFeatureSupported(supported.fillModeNonSolid, required.fillModeNonSolid) &&
        isFeatureSupported(supported.depthBounds, required.depthBounds) &&
        isFeatureSupported(supported.wideLines, required.wideLines) &&
        isFeatureSupported(supported.largePoints, required.largePoints) &&
        isFeatureSupported(supported.alphaToOne, required.alphaToOne) &&
        isFeatureSupported(supported.multiViewport, required.multiViewport) &&
        isFeatureSupported(supported.samplerAnisotropy, required.samplerAnisotropy) &&
        isFeatureSupported(supported.textureCompressionETC2, required.textureCompressionETC2) &&
        isFeatureSupported(supported.textureCompressionASTC_LDR, required.textureCompressionASTC_LDR) &&
        isFeatureSupported(supported.textureCompressionBC, required.textureCompressionBC) &&
        isFeatureSupported(supported.occlusionQueryPrecise, required.occlusionQueryPrecise) &&
        isFeatureSupported(supported.pipelineStatisticsQuery, required.pipelineStatisticsQuery) &&
        isFeatureSupported(supported.vertexPipelineStoresAndAtomics, required.vertexPipelineStoresAndAtomics) &&
        isFeatureSupported(supported.fragmentStoresAndAtomics, required.fragmentStoresAndAtomics) &&
        isFeatureSupported(supported.shaderTessellationAndGeometryPointSize, required.shaderTessellationAndGeometryPointSize) &&
        isFeatureSupported(supported.shaderImageGatherExtended, required.shaderImageGatherExtended) &&
        isFeatureSupported(supported.shaderStorageImageExtendedFormats, required.shaderStorageImageExtendedFormats) &&
        isFeatureSupported(supported.shaderStorageImageMultisample, required.shaderStorageImageMultisample) &&
        isFeatureSupported(supported.shaderStorageImageReadWithoutFormat, required.shaderStorageImageReadWithoutFormat) &&
        isFeatureSupported(supported.shaderStorageImageWriteWithoutFormat, required.shaderStorageImageWriteWithoutFormat) &&
        isFeatureSupported(supported.shaderUniformBufferArrayDynamicIndexing, required.shaderUniformBufferArrayDynamicIndexing) &&
        isFeatureSupported(supported.shaderSampledImageArrayDynamicIndexing, required.shaderSampledImageArrayDynamicIndexing) &&
        isFeatureSupported(supported.shaderStorageBufferArrayDynamicIndexing, required.shaderStorageBufferArrayDynamicIndexing) &&
        isFeatureSupported(supported.shaderStorageImageArrayDynamicIndexing, required.shaderStorageImageArrayDynamicIndexing) &&
        isFeatureSupported(supported.shaderClipDistance, required.shaderClipDistance) &&
        isFeatureSupported(supported.shaderCullDistance, required.shaderCullDistance) &&
        isFeatureSupported(supported.shaderFloat64, required.shaderFloat64) &&
        isFeatureSupported(supported.shaderInt64, required.shaderInt64) &&
        isFeatureSupported(supported.shaderInt16, required.shaderInt16) &&
        isFeatureSupported(supported.shaderResourceResidency, required.shaderResourceResidency) &&
        isFeatureSupported(supported.shaderResourceMinLod, required.shaderResourceMinLod) &&
        isFeatureSupported(supported.sparseBinding, required.sparseBinding) &&
        isFeatureSupported(supported.sparseResidencyBuffer, required.sparseResidencyBuffer) &&
        isFeatureSupported(supported.sparseResidencyImage2D, required.sparseResidencyImage2D) &&
        isFeatureSupported(supported.sparseResidencyImage3D, required.sparseResidencyImage3D) &&
        isFeatureSupported(supported.sparseResidency2Samples, required.sparseResidency2Samples) &&
        isFeatureSupported(supported.sparseResidency4Samples, required.sparseResidency4Samples) &&
        isFeatureSupported(supported.sparseResidency8Samples, required.sparseResidency8Samples) &&
        isFeatureSupported(supported.sparseResidency16Samples, required.sparseResidency16Samples) &&
        isFeatureSupported(supported.sparseResidencyAliased, required.sparseResidencyAliased) &&
        isFeatureSupported(supported.variableMultisampleRate, required.variableMultisampleRate) &&
        isFeatureSupported(supported.inheritedQueries, required.inheritedQueries);
}

inline void printMatrix(const glm::mat4& matrix, const std::string& name = "") {
    if (!name.empty()) {
        std::cout << name << std::endl;
    }
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            std::cout << matrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

inline void printVector(const glm::vec3& vector, const std::string& name = "") {
    if (!name.empty()) {
        std::cout << name << std::endl;
    }
    for (int i = 0; i < 3; ++i) {
        std::cout << vector[i] << " ";
    }
    std::cout << std::endl;
}

inline glm::mat4 computeNormalMatrix(const glm::mat4& modelMatrix) {
    return glm::transpose(glm::inverse(modelMatrix));
}


inline uint32_t findMemoryType(const uint32_t typeFilter, const VkMemoryPropertyFlags properties) {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(RendererState::inst().PhysicalDevice, &memProperties);

    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
        if ((typeFilter & (1 << i))
            && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }

    throw std::runtime_error("Failed to find suitable memory type!");
}

inline void createBuffer(
    const VkDeviceSize size,
    const VkBufferUsageFlags usage,
    const VkMemoryPropertyFlags properties,
    VkBuffer& buffer,
    VkDeviceMemory& bufferMemory) {
    // Create the buffer
    VkBufferCreateInfo bufferInfo{};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    checkResult(
        vkCreateBuffer(RendererState::inst().Device, &bufferInfo, nullptr, &buffer),
        "Failed to create vertex buffer!"
    );

    // Allocate and bind memory
    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(RendererState::inst().Device, buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

    checkResult(
        vkAllocateMemory(RendererState::inst().Device, &allocInfo, nullptr, &bufferMemory),
        "Failed to allocate vertex buffer memory!"
    );
    vkBindBufferMemory(RendererState::inst().Device, buffer, bufferMemory, 0);
}

inline VkCommandBuffer beginSingleTimeCommands() {
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = RendererState::inst().CommandPool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(RendererState::inst().Device, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    return commandBuffer;
}

inline void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    vkQueueSubmit(RendererState::inst().GraphicsQueue, 1, &submitInfo, nullptr);
    vkQueueWaitIdle(RendererState::inst().GraphicsQueue);

    vkFreeCommandBuffers(RendererState::inst().Device, RendererState::inst().CommandPool, 1, &commandBuffer);
}

inline void copyBuffer(const VkBuffer srcBuffer, const VkBuffer dstBuffer, const VkDeviceSize size) {
    const VkCommandBuffer commandBuffer = beginSingleTimeCommands();

    VkBufferCopy copyRegion{};
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

    endSingleTimeCommands(commandBuffer);
}

inline void createVertexBuffer(
    VkBuffer& vertexBuffer,
    VkDeviceMemory& vertexBufferMemory,
    const void* bufferData,
    const VkDeviceSize bufferSize) {
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        stagingBuffer,
        stagingBufferMemory
    );

    void* data;
    vkMapMemory(RendererState::inst().Device, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, bufferData, static_cast<size_t>(bufferSize));
    vkUnmapMemory(RendererState::inst().Device, stagingBufferMemory);

    createBuffer(
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        vertexBuffer,
        vertexBufferMemory
    );

    copyBuffer(stagingBuffer, vertexBuffer, bufferSize);
    vkDestroyBuffer(RendererState::inst().Device, stagingBuffer, nullptr);
    vkFreeMemory(RendererState::inst().Device, stagingBufferMemory, nullptr);
}

inline void createIndexBuffer(
    VkBuffer& indexBuffer,
    VkDeviceMemory& indexBufferMemory,
    const void* bufferData,
    const VkDeviceSize bufferSize) {

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        stagingBuffer,
        stagingBufferMemory
    );

    void* data;
    vkMapMemory(RendererState::inst().Device, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, bufferData, static_cast<size_t>(bufferSize));
    vkUnmapMemory(RendererState::inst().Device, stagingBufferMemory);

    createBuffer(
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        indexBuffer,
        indexBufferMemory
    );

    copyBuffer(stagingBuffer, indexBuffer, bufferSize);
    vkDestroyBuffer(RendererState::inst().Device, stagingBuffer, nullptr);
    vkFreeMemory(RendererState::inst().Device, stagingBufferMemory, nullptr);
}
