#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

struct Transform {
    glm::vec3 Position;
    /**
    * Rotation in radians
    */
    glm::vec3 RollPitchYaw;
    glm::vec3 Scale;

    explicit Transform(
            const glm::vec3& position = glm::vec3(),
            const glm::vec3& rollPitchYaw = glm::vec3(),
            const glm::vec3& scale = glm::vec3(1.0, 1.0, 1.0))
            : Position(position)
            , RollPitchYaw(rollPitchYaw)
            , Scale(scale) {
    }

    glm::mat4 generateModelMatrix() const {
        glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), RollPitchYaw.x, glm::vec3(1.0f, 0.0f, 0.0f));
        rotation = glm::rotate(rotation, RollPitchYaw.y, glm::vec3(0.0f, 1.0f, 0.0f));
        rotation = glm::rotate(rotation, RollPitchYaw.z, glm::vec3(0.0f, 0.0f, 1.0f));

        const glm::mat4 scale = glm::scale(glm::mat4(1.0f), Scale);
        const glm::mat4 translation = glm::translate(glm::mat4(1.0f), Position);
        return translation * rotation * scale;
    }
};
