#pragma once

#include <vulkan/vulkan.h>
#include <vector>
#include <glm/glm.hpp>
#include "Vertex.h"

class Box {
    VkBuffer vertexBuffer_ = nullptr;
    VkDeviceMemory vertexBufferMemory_ = nullptr;

    VkBuffer indexBuffer_ = nullptr;
    VkDeviceMemory indexBufferMemory_ = nullptr;

    uint32_t indexCount_ = 0;

public:
    static Box makeBox() {
        const std::vector<SimpleColorVertex> boxVertices = {
            { { -0.5f, -0.5f, -0.5f } },
            { { 0.5f, -0.5f, -0.5f } },
            { { 0.5f, 0.5f, -0.5f } },
            { { -0.5f, 0.5f, -0.5f } },

            { { -0.5f, -0.5f, 0.5f } },
            { { 0.5f, -0.5f, 0.5f } },
            { { 0.5f, 0.5f, 0.5f } },
            { { -0.5f, 0.5f, 0.5f } }
        };

        const std::vector<uint16_t> boxIndices = {
            6, 5, 4,
            4, 7, 6,

            0, 1, 2,
            2, 3, 0,

            2, 1, 0,
            0, 3, 2,

            2, 1, 5,
            5, 6, 2,

            0, 3, 7,
            7, 4, 0,

            3, 2, 6,
            6, 7, 3,

            0, 4, 5,
            5, 1, 0
        };

        Box box;

        createVertexBuffer(box.vertexBuffer_, box.vertexBufferMemory_, boxVertices.data(), boxVertices.size() * sizeof(boxVertices[0]));
        createIndexBuffer(box.indexBuffer_, box.indexBufferMemory_, boxIndices.data(), boxIndices.size() * sizeof(boxIndices[0]));
        box.indexCount_ = boxIndices.size();

        return box;
    }

    void draw(VkCommandBuffer commandBuffer) {
        VkBuffer vertexBuffers[] = { vertexBuffer_ };
        VkDeviceSize offsets[] = { 0 };
        vkCmdBindVertexBuffers(commandBuffer, 0, 1, vertexBuffers, offsets);
        vkCmdBindIndexBuffer(commandBuffer, indexBuffer_, 0, VK_INDEX_TYPE_UINT16);

        vkCmdDrawIndexed(commandBuffer, indexCount_, 1, 0, 0, 0);
    }

    void destroy() const {
        vkDestroyBuffer(RendererState::inst().Device, vertexBuffer_, nullptr);
        vkFreeMemory(RendererState::inst().Device, vertexBufferMemory_, nullptr);

        vkDestroyBuffer(RendererState::inst().Device, indexBuffer_, nullptr);
        vkFreeMemory(RendererState::inst().Device, indexBufferMemory_, nullptr);
    }
};
