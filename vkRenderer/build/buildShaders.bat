@echo off

SET mypath=%~dp0
SET shadersPath=%mypath%..\shaders\

:: Cleanup
if exist "%mypath%..\shaders\bin" RD /S /Q %mypath%..\shaders\bin
if not exist "%mypath%..\shaders\bin" mkdir %mypath%..\shaders\bin

:: Compile all shaders
for %%f in (%shadersPath%*.vert) do (
	%VULKAN_SDK%Bin\glslangValidator.exe -V -o %%~pfbin\%%~nf_vert.spv %%f
)

for %%f in (%shadersPath%*.frag) do (
	%VULKAN_SDK%Bin\glslangValidator.exe -V -o %%~pfbin\%%~nf_frag.spv %%f
)

