#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform sampler2D texSampler;

layout(binding = 2) uniform PointLight {
	vec3 Position;
	vec3 Color;
	vec3 ViewPos;
} pl;

layout(location = 0) in vec3 fragPosition;
layout(location = 1) in vec3 fragColor;
layout(location = 2) in vec3 fragNormal;
layout(location = 3) in vec2 fragTexCoord;

layout(location = 0) out vec3 outColor;

void main() {
	// Ambient
	vec3 fragColor = texture(texSampler, fragTexCoord).xyz;
	vec3 ambientColor = pl.Color * 0.2;

	// Diffuse
	vec3 normal = normalize(fragNormal);
	vec3 lightDir = normalize(pl.Position - fragPosition);
	float diffuseIntensity = max(dot(normal, lightDir), 0.0);
	vec3 diffuseColor = diffuseIntensity * pl.Color;

	// Specular
	vec3 viewDir = normalize(pl.ViewPos - fragPosition);
	vec3 halfVec = normalize(viewDir + lightDir);
	float specularIntensity = pow(dot(halfVec, normal), 32);
	vec3 specularColor = specularIntensity * pl.Color;

	outColor = (ambientColor + diffuseColor + specularColor) * fragColor;
	outColor = clamp(outColor, 0.0, 1.0);
}
