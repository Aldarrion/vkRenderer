#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 Model;
    mat4 View;
    mat4 Projection;
	vec3 Color;
} ubo;

layout(location = 0) in vec3 inPosition;

layout(location = 0) out vec3 fragColor;

void main() {
	gl_Position = ubo.Projection * ubo.View * ubo.Model * vec4(inPosition, 1);
	fragColor = ubo.Color;
}
