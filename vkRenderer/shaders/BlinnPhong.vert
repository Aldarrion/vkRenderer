#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 Model;
    mat4 View;
    mat4 Projection;
	mat4 NormalMatrix;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inTexCoord;

layout(location = 0) out vec3 fragPosition;
layout(location = 1) out vec3 fragColor;
layout(location = 2) out vec3 fragNormal;
layout(location = 3) out vec2 fragTexCoord;

void main() {
	gl_Position = ubo.Projection * ubo.View * ubo.Model * vec4(inPosition, 1);

	fragPosition = (ubo.Model * vec4(inPosition, 1)).xyz;
	fragColor = inColor;
	fragNormal = (ubo.NormalMatrix * vec4(inNormal, 1)).xyz;
	fragTexCoord = inTexCoord;
}
